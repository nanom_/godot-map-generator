extends Node2D

func _input(event: InputEvent) -> void:
	if Input.is_action_pressed("ui_down"):
		position.y += 100
	if Input.is_action_pressed("ui_up"):
		position.y -= 100
	if Input.is_action_pressed("ui_left"):
		position.x -= 100
	if Input.is_action_pressed("ui_right"):
		position.x += 100
