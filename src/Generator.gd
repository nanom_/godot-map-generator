extends Node2D

enum RoomType {
	SEA,
	BEACH,
	LAND,
	MOUNTAIN,
	MOUNTAIN_TOP,
}

enum Biome {
	PLAINS,
	FOREST,
	RIVER,
	TUNDRA,
	DESERT,
	SEA,
}

var map_width := 128
var map_height := 128
var map_area := map_width * map_height
var main_noise_map := OpenSimplexNoise.new()
var biome_noise_map := OpenSimplexNoise.new()
var map := Array()
var biome_map := Array()
var thread := Thread.new()
var world_ready := false
var sea_areas := 0
var mountain_areas := 0
var min_height := 10.0 # highest mountain room (default value is arbitrary)
var max_height := 0.0 # deepest sea room
var frames := 0

signal world_generated

func _ready() -> void:
	randomize()

	main_noise_map.seed = randi()
	main_noise_map.octaves = 7
	main_noise_map.period = 192

	biome_noise_map.seed = randi()
	biome_noise_map.octaves = 5
	biome_noise_map.period = 200

	connect("world_generated", self, "_on_world_generated")
	print2("Generating world...")
	thread.start(self, "generate_world", false)

func _input(event: InputEvent) -> void:
	if (Input.is_action_just_pressed("ui_select") and
		world_ready):
			get_tree().reload_current_scene()

func _on_world_generated() -> void:
	world_ready = true
	update()

func _draw() -> void:
	if !world_ready:
		return

	for x in map_width:
		for y in map_height:
			var color: Color
			match map[x][y]:
				RoomType.SEA:
					color = Color.blue
				RoomType.BEACH:
					color = Color.yellow
				RoomType.LAND:
					match biome_map[x][y]:
						Biome.PLAINS:
							color = Color.lime
						Biome.FOREST:
							color = Color.darkgreen
						Biome.RIVER:
							color = Color.blue
						Biome.TUNDRA:
							color = Color.purple
						Biome.DESERT:
							color = Color.goldenrod
						Biome.SEA: # this shouldn't happen but it happens lol
							biome_map[x][y] = Biome.PLAINS
							color = Color.lime
				RoomType.MOUNTAIN:
					color = Color.darkgray
				RoomType.MOUNTAIN_TOP:
					color = Color.white
			draw_rect(Rect2(x * 2, y * 2, 2, 2), color)

func _process(_delta: float) -> void:
	if !world_ready:
		frames += 1
	if $Label.text.split("\n").size() > 30:
		$Label.text = ""

func _exit_tree() -> void:
	thread.wait_to_finish()

func generate_world(__: bool) -> void:
	generate_base_map()
	print2("Ready.")
	emit_signal("world_generated")

	var secs := frames / 60.0
	print2("---")
	print2("World generated (~" + str(secs) + "s). Sea rooms: " + str(sea_areas))
	print2("Highest room: " + str(lerp(
		-10000, 9000, max_height / 2
	)) + "m")

func generate_base_map() -> void:
	print2("Generating height map and biomes...")
	for x in map_width:
		map.push_back(Array())
		biome_map.push_back(Array())
		for y in map_height:
			var randnum := abs(main_noise_map.get_noise_2d(x, y) * 3)
			var randnum2 := abs(biome_noise_map.get_noise_2d(x, y))
			var biome_set := false

			if randnum < min_height:
				min_height = randnum
			if randnum > max_height:
				max_height = randnum

			if randnum < .5:
				map[x].push_back(RoomType.SEA)
				biome_map[x].push_back(Biome.SEA)
				biome_set = true
				sea_areas += 1
			elif randnum <= .51:
				map[x].push_back(RoomType.BEACH)
			elif randnum > 1.5:
				map[x].push_back(RoomType.MOUNTAIN_TOP)
			elif randnum > 1.1:
				map[x].push_back(RoomType.MOUNTAIN)
				mountain_areas += 1
			else:
				map[x].push_back(RoomType.LAND)

			if !biome_set:
				if randnum2 <= .06:
					biome_map[x].push_back(Biome.DESERT)
				elif randnum2 <= .2:
					biome_map[x].push_back(Biome.PLAINS)
				elif randnum2 <= .22:
					biome_map[x].push_back(Biome.RIVER)
				elif randnum2 <= .33:
					biome_map[x].push_back(Biome.PLAINS)
				elif randnum2 <= .43:
					biome_map[x].push_back(Biome.FOREST)
				else:
					biome_map[x].push_back(Biome.TUNDRA)

	if sea_areas > map_area / 2:
		print2("Too much water (" + str(sea_areas) + ")! Trying another seed...")
		restart_generation()
		return

	if mountain_areas < 500:
		print2("Very small or no mountains! Trying another seed...")
		restart_generation()
		return

func restart_generation() -> void:
	map.clear()
	sea_areas = 0
	mountain_areas = 0
	max_height = 0.0
	min_height = 0.0
	main_noise_map.seed = randi()
	generate_base_map()

func print2(text: String) -> void:
	$Label.text += text + "\n"
