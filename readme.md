Map generator
---
This is a random map generator written in GDScript. It generates maps with
oceans, mountains, lakes, rivers and biomes.

License
---
The source code is in the public domain. Use it if you want. Credit is not
required.
